//comentar
/* 

    Faça um programa que receba um número positivo e
    maior que zero, calcule e mostre:
    a) o numero digitado ao quadrado;
    b) o numero digitado ao cubo;
    c) a raiz quadrada do numero digitado;
    d) a raiz cubica de numero digitado
    */

//inicio
    namespace exercicio_11 {
  //entrada de dados
  //let numero: number;
  const numero = 67;
  let numQ: number;
  numQ = numero * numero;
  let numC: number;
  numC = numero * numero * numero;
  numC = Math.pow(numero, 3);
  numC = numero ** 34;
  let raizQ: number;
  raizQ = Math.sqrt(numero);
  let raizC: Number;
  raizC = Math.cbrt(numero);
  console.log(
    `O numero elevado ao quadrado: ${numQ} 
    \n o numero elevado ao cubo: ${numC} 
    \n a raiz quadrada do numero: ${raizQ} 
    \n a raiz cubica do numero ${raizC}`
  );
}
